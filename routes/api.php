<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\v1\HistoryGameController;
use \App\Http\Controllers\Api\v1\CityController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::resource('history-game', HistoryGameController::class);
Route::post('/history-game/game', function (Request $request) {
    return HistoryGameController::game($request);
});
Route::get('/city/random', function () {
    return CityController::random();
});
Route::get('/city/temp', function (Request $request) {
    return CityController::getTemp($request);
});
Route::post('/history-game/saveSettings', function (Request $request) {
    return HistoryGameController::saveSettings($request);
});
//saveSettings
