import vueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(vueRouter);

import GameStartComponent from "./components/GameStartComponent";
import GameComponent from "./components/GameComponent";
import HistoryGameComponent from "./components/HistoryGameComponent";

const routes = [
    {
        path: "/",
        component: GameStartComponent
    },
    {
        path: "/game",
        component: GameComponent
    },
    {
        path: "/settings",
        component: HistoryGameComponent
    },
];


export default new vueRouter({
    mode: "history",
    routes
});
