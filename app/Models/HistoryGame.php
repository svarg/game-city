<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryGame extends Model
{
    use HasFactory;
    protected $table = 'history_game';
    protected $fillable = ['name', 'score','type','games'];
}
