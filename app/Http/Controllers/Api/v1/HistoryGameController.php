<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\HistoryGame;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HistoryGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(HistoryGame::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request->post("name");

        $validator = \Illuminate\Support\Facades\Validator::make(
            $request->all(),
            [
                "name" => ["required"],
                "score" => ["required"],
                "type" => ["required"]
            ]
        );

        if ($validator->fails()) {
            return [
                "status" => false,
                "errors" => $validator->messages()
            ];
        }

        $game = HistoryGame::create([
            "name" => $request->post("name"),
            "score" => $request->post("score"),
            "type" => $request->post("type"),
        ]);
        $request->session()->start();
        $request->session()->put("id", $game->id);
        $request->session()->put("name", $request->post("name"));
        $request->session()->put("score", $request->post("score"));
        $request->session()->put("type", $request->post("type"));

        return [
            "status" => true,
            "post" => $game
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = HistoryGame::find($id);
        if (!$game) {
            return response()->json([
                "status" => false,
                "message" => "Post not found"
            ])->setStatusCode(404);
        }

        return $game;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $game = HistoryGame::find($id);
        $game->name = $request->post("name");
        $game->score = $request->post("score");
        $game->type = $request->post("type");
        if ($game->games) {
            $arGames = json_decode($game->games);
            array_push($arGames, $request->post("cities"));
            $game->games = json_encode($arGames);
        } else {
            $arGames = [];
            array_push($arGames, $request->post("cities"));
            $game->games = json_encode($arGames);
        }
        $game->save();

        $request->session()->start();
        $request->session()->put("id", $id);
        $request->session()->put("name", $game->name);
        $request->session()->put("score", $game->score);
        $request->session()->put("type", $game->type);
        return [
            "status" => true,
            "post" => $game
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function game(Request $request)
    {
        $result = $request->session()->all();
        $game = HistoryGame::find($request->session()->get('id'));
        $result['games'] = $game->games;
        return $result;
    }

    public static function saveSettings(Request $request){
        $game = HistoryGame::find($request->session()->get('id'));
        $game->type = $request->post("type");
        $game->save();
        $request->session()->put("type", $game->type);
        return [
            "status" => true,
            "post" => $game
        ];
    }
}
