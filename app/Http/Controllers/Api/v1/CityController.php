<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CityController extends Controller
{
    public static function random(){
        $cities = City::all('id','name')->random(2);
        return response()->json($cities);
    }
    public static function getTemp(Request $request){
        return Http::get($request->get('url'));
    }
}
