<?php

namespace App\Console\Commands;

use App\Models\City;
use Illuminate\Console\Command;

class ParserCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:parser-cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cities = json_decode(file_get_contents('./resources/json/city.list.json'));
        $count = 0;
        foreach ($cities as $city) {
            City::create([
                'name'=>$city->name,
                'state'=>$city->state,
                'country'=>$city->country,
                'id'=>$city->id,
            ]);
            $count++;
        }
        return $count;
    }
}
